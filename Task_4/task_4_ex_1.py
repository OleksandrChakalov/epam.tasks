"""04 Task 1.1
Implement a function which receives a string and replaces all " symbols with ' and vise versa. The
function should return modified string.
Usage of any replacing string functions is prohibited.
"""


def swap_quotes(string: str) -> str:
    """Quotation marks changer

    A function which receives a string and replaces all " symbols with ' and vise versa.
    :param string:
    :return:
    """
    ns = list(string)
    for ind in range(len(ns)):
        if ns[ind] == "\'":
            ns[ind] = "\""
        elif ns[ind] == "\"":
            ns[ind] = "\'"
    return "".join(ns)
