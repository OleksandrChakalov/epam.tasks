"""
For a positive integer n calculate the result value, which is equal to the sum
of the odd numbers of n.

Example,
n = 1234 result = 4
n = 246 result = 0

Write it as function.

Note:
Raise TypeError in case of wrong data type or negative integer;
Use of 'functools' module is prohibited, you just need simple for loop.
"""


def sum_odd_numbers(n: int) -> int:
    """Odd numbers calculator

    For a positive integer n calculate the result value, which is equal to the sum
    of the odd numbers of n.
    :param n:
    :return:
    """

    for i in str(n):
        if not i.isdigit() or int(n) <= 0:
            raise TypeError
    n = [int(d) for d in str(n)]
    return sum(num for num in n if num % 2 == 1)
