"""
Task04_3

Implement a function which works the same as str.split

Note:
Usage of str.split method is prohibited
Raise ValueError in case of wrong data type
"""


def split_alternative(str_to_split: str, delimiter=" ") -> list:
    """Split analog

    A function which works the same as str.split
    :param str_to_split:
    :param delimiter:
    :return:
    """
    if not isinstance(str_to_split, str):
        raise ValueError
    splitted = []
    j = 0
    for i in range(len(str_to_split)):
        if str_to_split[i] == delimiter:
            splitted.append(str_to_split[j:i])
            j = i + 1
    splitted.append(str_to_split[j:])
    return splitted
