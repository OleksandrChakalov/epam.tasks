"""
Implement a function `split_by_index(string: str, indexes: List[int]) -> List[str]`
which splits the `string` by indexes specified in `indexes`. 
Only positive index, larger than previous in list is considered valid.
Invalid indexes must be ignored.

Examples:
```python
>>> split_by_index("pythoniscool,isn'tit?", [6, 8, 12, 13, 18])
["python", "is", "cool", ",", "isn't", "it?"]

>>> split_by_index("pythoniscool,isn'tit?", [6, 8, 8, -4, 0, "u", 12, 13, 18])
["python", "is", "cool", ",", "isn't", "it?"]

>>> split_by_index("no luck", [42])
["no luck"]
```
"""


def split_by_index(string, indexes):
    """Splitting function (by indexes)

    Function which splits the `string` by indexes specified in `indexes`.
    :param string:
    :param indexes:
    :return:
    """
    max_elem = 0
    new_indexes = []
    splited = []
    for elem in indexes:
        if not isinstance(elem, int) or elem > len(string):
            continue
        if elem > max_elem:
            max_elem = elem
            new_indexes.append(elem)
    k = 0
    for elem in new_indexes:
        splited.append(string[k:elem])
        k = elem
    splited.append(string[k:])
    return splited



