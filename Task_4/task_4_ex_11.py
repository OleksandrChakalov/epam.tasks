"""
Write a function `fibonacci_loop(seq: list)`, which accepts a list of values and
prints out values in one line on these conditions:
 - floating point numbers should be ignored
 - string values should stop the iteration
 - loop control statements should be used

Example:
>>> fibonacci_loop([0, 1, 1.1, 1, 2, 99.9, 3, 0.0, 5, 8, "stop", 13, 21, 34])
0 1 1 2 3 5 8
"""



def fibonacci_loop(seq):
    """Take only fibonacci values from the list

    A function `fibonacci_loop(seq: list)`, which accepts a list of values and
    prints out values in one line
    :param seq:
    :return:
    """
    i = 0
    fibonacci = []
    while not isinstance(seq[i], str):           #stop function if string founded
        if not isinstance(seq[i], float):        #float ignoring
            fibonacci.append(seq[i])
        i += 1
    print(*fibonacci)


