"""
Create a function sum_binary_1 that for a positive integer n
calculates the result value, which is equal to the sum of the
“1” in the binary representation of n otherwise, returns None.

Example,
n = 14 = 1110 result = 3
n = 128 = 10000000 result = 1
"""


def sum_binary_1(n: int):
    """sum_binary calculator

    a function sum_binary_1 that for a positive integer n
    calculates the result value, which is equal to the sum of the
    “1” in the binary representation of n otherwise, returns None.
    """
    import math
    rem = ""
    if isinstance(n, int):
        while n >= 1:
            rem += str(n % 2)
            n = math.floor(n / 2)

        binary = ""
        for i in range(len(rem) - 1, -1, -1):
            binary = binary + rem[i]

        n = [int(d) for d in str(binary)]
        if sum(n) == 0:
            return None
        return sum(n)
