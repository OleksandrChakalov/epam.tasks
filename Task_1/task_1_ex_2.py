"""01-Task1-Task2
Write a Python-script that performs the standard math functions on the data. The name of function and data are
set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py add 1 2

Notes:
Function names must match the standard mathematical, logical and comparison functions from the built-in libraries.
The script must raises all happened exceptions.
For non-mathematical function need to raise NotImplementedError.
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""
import argparse
import math
import operator


def calculate(args):
    """Standard math functions on the data.

    Python-script that performs the standard math functions on the data. The name of function and data are
    set on the command line when the script is run.
    With using the argparse module to parse command line arguments.
    """
    user_input = args.expression[0] + '(' + ','.join(args.expression[1:]) + ')'
    if args.expression[0] not in dir(math) and args.expression[0] not in dir(operator):
        raise NotImplementedError
    if args.expression[0] in dir(math):
        user_input = 'math.' + user_input

    else:
        user_input = 'operator.' + user_input

    return eval(user_input)


def main():
    parser = argparse.ArgumentParser("Make some simple actions")
    parser.add_argument("expression", nargs='*')
    args = parser.parse_args()
    print(calculate(args))


if __name__ == '__main__':
    main()
