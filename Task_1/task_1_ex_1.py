"""01-Task1-Task1
Write a Python-script that performs simple arithmetic operations: '+', '-', '*', '/'. The type of operator and
data are set on the command line when the script is run.
The script should be launched like this:
$ python my_task.py 1 * 2

Notes:
For other operations need to raise NotImplementedError.
Do not dynamically execute your code (for example, using exec()).
Use the argparse module to parse command line arguments. Your implementation shouldn't require entering any
parameters (like -f or --function).
"""
import argparse

parser = argparse.ArgumentParser(description="Make some simple actions")
parser.add_argument("elem1", type=float)
parser.add_argument("action", type=str)
parser.add_argument("elem2", type=float)


def calculate(args):
    """Performs simple arithmetic operations.

    Python function that performs simple arithmetic operations: '+', '-', '*', '/'.
    With using the agrparse module to parse command line arguments.
    The script should be launched like this:
    $ python my_task.py 1 * 2
    """
    action = args.action
    elem1 = args.elem1
    elem2 = args.elem2
    simple_math = {

        '+': lambda el1, el2: el1 + el2,
        '-': lambda el1, el2: el1 - el2,
        '/': lambda el1, el2: el1 / el2,
        '*': lambda el1, el2: el1 * el2

    }
    if answer := simple_math.get(action):
        return answer(elem1, elem2)
    raise NotImplementedError


def main():
    args = parser.parse_args()
    print(calculate(args))


if __name__ == '__main__':
    main()
