import argparse

parser = argparse.ArgumentParser()
parser.add_argument("formula")
args = parser.parse_args()
formula = args.formula
sign = ['+', '-', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

if __name__ == '__main__':
    for i in range(0, len(formula) - 1):
        """Determines whether the input string is correct according to EBNF.
        
        Formula = Number | (Formula Sign Formula)
        Sign = '+' | '-'
        Number = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' 
        Input = string
        Result = (True / False, The result value / None)
        """

        if formula[i] not in sign:
            print("(False, None)")
            break
        if formula[i] == sign[0] or formula[i] == sign[1]:  # перевірка чи є символи які повторюються
            if formula[i + 1] == sign[0] or formula[i + 1] == sign[1]:
                print("(False, None)")
                break
    else:
        try:
            print("(True," + str(eval(formula)) + ")")
        except:
            print("(False, None)")  # перевірка на помилки які могли бути пропущені
