""" Write a Python-script that determines whether the input string is the correct entry for the
'formula' according EBNF syntax (without using regular expressions).
Formula = Number | (Formula Sign Formula)
Sign = '+' | '-'
Number = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
Input: string
Result: (True / False, The result value / None)

Example,
user_input = '1+2+4-2+5-1' result = (True, 9)
user_input = '123' result = (True, 123)
user_input = 'hello+12' result = (False, None)
user_input = '2++12--3' result = (False, None)
user_input = '' result = (False, None)

Example how to call the script from CLI:
python task_1_ex_3.py 1+5-2

Hint: use argparse module for parsing arguments from CLI
"""
import argparse


def check_formula(user_input):
    """EBNF syntax script.

    Python-script that determines whether the input string is the correct entry for the
    'formula' according EBNF syntax:
    Formula = Number | (Formula Sign Formula)
    Sign = '+' | '-'
    Number = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
    Input: string
    Result: (True / False, The result value / None)
    """
    user_input = user_input
    num = 0
    state = True

    for s in user_input.user_input:
        if num > 1:
            state = False
            print('(False, None)')
            break
        if not s.isdigit():
            num += 1
        else:
            num = 0
    try:
        if state:
            print('(True, ' + str(eval(user_input.user_input)) + ')')
    except Exception:
        print('(False, None)')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('user_input')
    user_input = parser.parse_args()
    check_formula(user_input)


if __name__ == '__main__':
    main()
