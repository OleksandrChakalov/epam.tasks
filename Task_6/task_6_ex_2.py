"""
Create CustomList – the linked list of values of random type, which size changes dynamically and has an ability to index
elements.

The task requires implementation of the following functionality:
• Create the empty user list and the one based on enumeration of values separated by commas. The elements are stored
in form of unidirectional linked list. Nodes of this list must be implemented in class Item.
    Method name: __init__(self, *data) -> None;
• Add and remove elements.
    Method names: append(self, value) -> None - to add to the end,
                add_start(self, value) -> None - to add to the start,
                remove(self, value) -> None - to remove the first occurrence of given value;
• Operations with elements by index. Negative indexing is not necessary.
    Method names: __getitem__(self, index) -> Any,
                __setitem__(self, index, data) -> None,
                __delitem__(self, index) -> None;
• Receive index of predetermined value.
    Method name: find(self, value) -> Any;
• Clear the list.
    Method name: clear(self) -> None;
• Receive lists length.
    Method name: __len__(self) -> int;
• Make CustomList iterable to use in for-loops;
    Method name: __iter__(self);
• Raise exceptions when:
    find() or remove() don't find specific value
    index out of bound at methods __getitem__, __setitem__, __delitem__.


Notes:
    The class CustomList must not inherit from anything (except from the object, of course).
    Function names should be as described above. Additional functionality has no naming requirements.
    Indexation starts with 0.
    List length changes while adding and removing elements.
    Inside the list the elements are connected as in a linked list, starting with link to head.
"""
from typing import Any


class Item:
    """
    A node in a unidirectional linked list.
    """

    def __init__(self, *data):
        self.item = data
        self.ref = None


class CustomList:
    """
    An unidirectional linked list.
    """

    def __init__(self, *args):
        """Constructor to initiate this object"""
        self.d_list = []
        self.it_val = 0
        for a in args:
            self.d_list.append(a)

    def add_start(self, value) -> None:
        """To add an element to the start

        :param value:
        :return:
        """
        self.d_list.insert(0, value)

    def append(self, value) -> None:
        "add an item at the end of the list"
        self.d_list.append(value)

    def remove(self, value) -> None:
        """To remove the first occurrence of given value

        :param value:
        :return:
        """
        try:
            self.d_list.remove(value)
        except:
            raise ValueError

    def __len__(self) -> int:
        """Receive lists length

        :return:
        """
        return len(self.d_list)

    def __getitem__(self, index) -> Any:
        """Operation with elements by index

        :param index:
        :return:
        """
        if index < 0 or index >= self.__len__():
            raise IndexError
        return self.d_list[index]

    def __setitem__(self, index, data) -> None:
        """Operation with elements by index

        :param index:
        :param data:
        :return:
        """
        if index < 0 or index > self.__len__():
            raise IndexError
        self.d_list.insert(index, data)

    def __delitem__(self, index) -> None:
        """Operation with elements by index

        :param index:
        :return:
        """
        if index < 0 or index >= self.__len__():
            raise IndexError
        del self.d_list[index]

    def find(self, value) -> Any:
        """Receive index of predetermined value

        :param value:
        :return:
        """
        try:
            return self.d_list.index(value)
        except:
            raise ValueError

    def clear(self) -> None:
        """Clear the list

        :return:
        """
        self.d_list = []

    def __iter__(self):
        """Make CustomList iterable to use in for-loops"""
        return self

    def __next__(self):
        temp = self.it_val
        self.it_val += 1
        if temp < self.__len__():
            return self.d_list[temp]
        else:
            self.it_val = 0
            raise StopIteration
