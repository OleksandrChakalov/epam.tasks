"""
Implement function combine_dicts, which receives a changeable
number of dictionaries (keys - letters, values - integers)
and combines them into one dictionary.

Dict values should be summarized in case of identical keys.

Example:
dict_1 = {'a': 100, 'b': 200}
dict_2 = {'a': 200, 'c': 300}
dict_3 = {'a': 300, 'd': 100}

combine_dicts(dict_1, dict_2)
Result: {'a': 300, 'b': 200, 'c': 300}

combine_dicts(dict_1, dict_2, dict_3)
Result: {'a': 600, 'b': 200, 'c': 300, 'd': 100}
"""
from string import ascii_lowercase


def combine_dicts(*args):
    """Combine dictionaries together

    Function combine_dictsreceives a changeable
    number of dictionaries (keys - letters, values - integers)
    and combines them into one dictionary.
    Dict values are summarized in case of identical keys.
    :param args:
    :return:
    """
    new_dict = dict()
    for dicts in args:
        for key in dicts:
            if key not in list(ascii_lowercase):  # check if key is a letter
                raise KeyError
            if not isinstance(dicts[key], int):  # check for correct datatype of values
                raise ValueError
            new_dict[key] = new_dict.get(key, 0) + dicts.get(key, 0)

    return new_dict
