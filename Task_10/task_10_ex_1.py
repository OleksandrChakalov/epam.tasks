"""
Implement a function `sort_names(input_file_path: str, output_file_path: str) -> None`, which sorts names from
`file_path` and write them to a new file `output_file_path`. Each name should start with a new line as in the
following example:
Example:

Adele
Adrienne
...
Willodean
Xavier
"""


def sort_names(input_file_path: str, output_file_path: str) -> None:
    """Sorts names from `file_path` and write them to a new file `output_file_path`."""
    names = []
    with open(input_file_path, 'r') as file:
        for word in file:
            names.append(word)
    names.sort()
    for sort_word in names:
        with open(output_file_path, 'a+') as f:
            f.write(sort_word)
