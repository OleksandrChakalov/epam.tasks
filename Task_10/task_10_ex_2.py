"""Implement a function `most_common_words(file_path: str, top_words: int) -> list`
which returns most common words in the file.
<file_path> - system path to the text file
<top_words> - number of most common words to be returned

Example:

print(most_common_words(file_path, 3))
>>> ['donec', 'etiam', 'aliquam']
> NOTE: Remember about dots, commas, capital letters etc.
"""
from collections import Counter

def most_common_words(file_path: str, top_words: int) -> list:
    """Function which returns most common words in the file."""
    words = []
    with open(file_path, 'r') as file_input:
        for word in file_input.read().split():
            while not word.isalpha() and word:
                word = word[0:-1]
            if word:
                words.append(word)
    return [x[0] for x in Counter(words).most_common(top_words)]
