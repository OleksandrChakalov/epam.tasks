"""
Task 3

Implement a decorator `call_once` which runs `sum_of_numbers` function once and caches the result.
All consecutive calls to this function should return cached result no matter the arguments.

Example:
@call_once
def sum_of_numbers(a, b):
    return a + b

print(sum_of_numbers(13, 42))

>>> 55

print(sum_of_numbers(999, 100))

>>> 55

print(sum_of_numbers(134, 412))

>>> 55
"""


def call_once(function):
    """runs `sum_of_numbers` function once and caches the result"""
    cache = None
    count = 1
    cap = None

    def wrap(*args):
        nonlocal count, cap, cache
        if count == 1:
            cache = function(*args)
            count += 1
            cap = cache
        else:
            cap = cache
        return cap

    return wrap


@call_once
def sum_of_numbers(a, b):
    return a + b


