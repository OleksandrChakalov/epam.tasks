"""
Implement function count_letters, which takes string as an argument and
returns a dictionary that contains letters of given string as keys and a
number of their occurrence as values.

Example:
print(count_letters("Hello world!"))
Result: {'H': 1, 'e': 1, 'l': 3, 'o': 2, 'w': 1, 'r': 1, 'd': 1}

Note: Pay attention to punctuation.
"""


def count_letters(string):
    """Function which takes string as an argument and returns a dictionary that contains letters of given string"""
    if isinstance(string, str):
        your_dict = {}
        string = ''.join(e for e in string if e.isalnum())
        for i in string:
            if i in your_dict:
                your_dict[i] += 1
            else:
                your_dict[i] = 1
        return your_dict
    else:
        raise TypeError


print(count_letters("Hello world!"))
