"""
Implement a bunch of functions which receive a changeable number of strings and return next
parameters:
1) characters that appear in all strings
2) characters that appear in at least one string
3) characters that appear at least in two strings
  Note: raise ValueError if there are less than two strings
4) characters of alphabet, that were not used in any string
  Note: use `` for list of alphabet letters

Note: raise TypeError in case of wrong data type

Examples,
```python
test_strings = ["hello", "world", "python", ]
print(chars_in_all(*test_strings))
>>> {'o'}
print(chars_in_one(*test_strings))
>>> {'d', 'e', 'h', 'l', 'n', 'o', 'p', 'r', 't', 'w', 'y'}
print(chars_in_two(*test_strings))
>>> {'h', 'l', 'o'}
print(not_used_chars(*test_strings))
>>> {'q', 'k', 'g', 'f', 'j', 'u', 'a', 'c', 'x', 'm', 'v', 's', 'b', 'z', 'i'}
"""

import string


def chars_in_all(*strings):
    """Characters that appear in all strings"""
    all_letters = []
    for word in strings:
        all_letters.append(list(word))

    for i in range(len(strings) - 1):
        all_letters = list(set(strings[i]) & set(strings[i + 1]))
    return set(all_letters)


def chars_in_one(*strings):
    """Characters that appear in at least one string"""
    # all_letters = []
    # for word in strings:
    #     for letter in word:
    #         all_letters.append(letter)
    # return set(all_letters)
    return set(''.join(strings))


def chars_in_two(*strings):
    """Characters that appear at least in two strings"""
    if len(strings) < 2:
        raise ValueError

    sets = []
    for word in strings:
        if not isinstance(word, str):
            raise TypeError
        for word2 in strings:
            if word != word2:
                sets.append(set(word) & set(word2))
    return set().union(*sets)


def not_used_chars(*strings):
    """Characters of alphabet, that were not used in any string"""
    all_letters = list(string.ascii_lowercase)
    final = []
    for word in strings:
        for letter in word:
            final.append(letter)

    return set(set(all_letters) - set(final))
