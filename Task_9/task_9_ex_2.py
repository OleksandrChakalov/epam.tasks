"""
Write a function that checks whether a string is a palindrome or not.
Return 'True' if it is a palindrome, else 'False'.

Note:
Usage of reversing functions is required.
Raise ValueError in case of wrong data type

To check your implementation you can use strings from here
(https://en.wikipedia.org/wiki/Palindrome#Famous_palindromes).
"""
import re

def is_palindrome(test_string: str) -> bool:
    """A function that checks whether a string is a palindrome or not"""
    if isinstance(test_string, str):
        test_string = test_string.lower()
        test_string = re.sub('[^A-Za-z0-9]+', '', test_string)
        print(test_string)
        rev = ''.join(reversed(test_string))
        return rev == test_string
    else:
        raise ValueError




