"""
Task_9_1
Implement `swap_quotes` function which receives a string and replaces all " symbols with ' and vise versa.
The function should return modified string.

Note:
Usage of built-in or string replacing functions is required.
"""

from functools import reduce


def swap_quotes(some_string: str) -> str:
    """Function which receives a string and replaces all " symbols with ' and vise versa"""
    char_to_replace = {'\'': '\"',
                       '\"': '\''}
    some_string = some_string.translate(str.maketrans(char_to_replace))

    return some_string



